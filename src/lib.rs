//  gst-plugin-sdr — a GStreamer plugin to handle software defined radio.
//
//  Copyright © 2018, 2019  Russel Winder <russel@winder.org.uk>
//
//  gst-plugin-sdr is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  gst-plugin-sdr is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with gst-plugin-sdr.  If not, see <https://www.gnu.org/licenses/>.

#![crate_type = "cdylib"]

// Even though this is supposed to be a Rust 2018 build, the statements:
//     use glib::glib_result_from_gboolean;
//     use gstreamer as gst;
//     use gst::{gst_error, gst_plugin_define};
// do not work – ends up with being fails in the gst_plugin_define. Instead
// the old Rust 2015 mechanism must be used. :-( :-(

#[macro_use]
extern crate glib;
#[macro_use]
extern crate gstreamer as gst;

mod dab_channel;
mod iq_demodulator;
mod soapysdrsrc;
mod swradiosrc;
mod src_common;

fn plugin_init(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    iq_demodulator::register(plugin)?;
    soapysdrsrc::register(plugin)?;
    swradiosrc::register(plugin)?;
    Ok(())
}

gst_plugin_define!(
    sdr,
    env!("CARGO_PKG_DESCRIPTION"),
    plugin_init,
    env!("CARGO_PKG_VERSION"), // concat!(env!("CARGO_PKG_VERSION"), "-", env!("COMMIT_ID")),
    "LGPL",
    env!("CARGO_PKG_NAME"),
    env!("CARGO_PKG_NAME"),
    env!("CARGO_PKG_REPOSITORY"),
    "2019-06-03" // env!("BUILD_REL_DATE")
);
