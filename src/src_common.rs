//  gst-plugin-sdr — a GStreamer plugin to handle software defined radio.
//
//  Copyright © 2019  Russel Winder <russel@winder.org.uk>
//
//  gst-plugin-sdr is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  gst-plugin-sdr is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with gst-plugin-sdr.  If not, see <https://www.gnu.org/licenses/>.

use glib::subclass::Property;

pub static PROPERTIES: [Property; 1] = [
    Property("frequency", |x| {
        glib::ParamSpec::uint(
            "frequency",
            "Frequency",
            "Frequency",
            1_000,
            1_000_000,
            105_400,
            glib::ParamFlags::READWRITE,
        )
    }),
];

#[derive(Clone, Debug, Default)]
pub struct Settings {
    pub frequency: u64,
    pub sampling_rate: f32,
}

#[derive(Debug, PartialEq)]
pub enum StreamingState {
    Stopped,
    Started,
}

#[cfg(test)]
mod tests {

    use super::Settings;

    #[test]
    fn default_settings_sets_expected_values() {
        let settings: Settings = Default::default();
        assert_eq!(settings.frequency, 0);
        assert_eq!(settings.sampling_rate, 0.0);
    }

}
