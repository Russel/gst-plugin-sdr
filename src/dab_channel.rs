//  gst-plugin-sdr — a GStreamer plugin to handle software defined radio.
//
//  Copyright © 2018 Russel Winder <russel@winder.org.uk>
//
//  gst-plugin-sdr is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  gst-plugin-sdr is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with gst-plugin-sdr.  If not, see <https://www.gnu.org/licenses/>.

/// An enumeration of all the defined DAB channels.
#[allow(non_camel_case_types)]  // To allow the _ in the variant name.
#[allow(dead_code)]  // To avoid warnings of unused variants.
enum DABChannel {
    C_5A,
    C_5B,
    C_5C,
    C_5D,
    C_6A,
    C_6B,
    C_6C,
    C_6D,
    C_7A,
    C_7B,
    C_7C,
    C_7D,
    C_8A,
    C_8B,
    C_8C,
    C_8D,
    C_9A,
    C_9B,
    C_9C,
    C_9D,
    C_10A,
    C_10B,
    C_10C,
    C_10D,
    C_11A,
    C_11B,
    C_11C,
    C_11D,
    C_12A,
    C_12B,
    C_12C,
    C_12D,
    C_13A,
    C_13B,
    C_13C,
    C_13D,
    C_13E,
    C_13F,

    C_LA,
    C_LB,
    C_LC,
    C_LD,
    C_LE,
    C_LF,
    C_LG,
    C_LH,
    C_LI,
    C_LJ,
    C_LK,
    C_LL,
    C_LM,
    C_LN,
    C_LO,
    C_LP,
}

impl DABChannel {
    /// Return the frequency of the DAB channel in kHz.
    fn frequency(&self) -> u64 {
        match *self {
            DABChannel::C_5A => 174_928,
            DABChannel::C_5B => 176_640,
            DABChannel::C_5C => 178_352,
            DABChannel::C_5D => 180_064,
            DABChannel::C_6A => 181_936,
            DABChannel::C_6B => 183_648,
            DABChannel::C_6C => 185_360,
            DABChannel::C_6D => 187_072,
            DABChannel::C_7A => 188_928,
            DABChannel::C_7B => 190_640,
            DABChannel::C_7C => 192_352,
            DABChannel::C_7D => 194_064,
            DABChannel::C_8A => 195_936,
            DABChannel::C_8B => 197_648,
            DABChannel::C_8C => 199_360,
            DABChannel::C_8D => 201_072,
            DABChannel::C_9A => 202_928,
            DABChannel::C_9B => 204_640,
            DABChannel::C_9C => 206_352,
            DABChannel::C_9D => 208_064,
            DABChannel::C_10A => 209_936,
            DABChannel::C_10B => 211_648,
            DABChannel::C_10C => 213_360,
            DABChannel::C_10D => 215_072,
            DABChannel::C_11A => 216_928,
            DABChannel::C_11B => 218_640,
            DABChannel::C_11C => 220_352,
            DABChannel::C_11D => 222_064,
            DABChannel::C_12A => 223_936,
            DABChannel::C_12B => 225_648,
            DABChannel::C_12C => 227_360,
            DABChannel::C_12D => 229_072,
            DABChannel::C_13A => 230_784,
            DABChannel::C_13B => 232_496,
            DABChannel::C_13C => 234_208,
            DABChannel::C_13D => 235_776,
            DABChannel::C_13E => 237_448,
            DABChannel::C_13F => 239_200,

            DABChannel::C_LA => 1_452_960,
            DABChannel::C_LB => 1_454_672,
            DABChannel::C_LC => 1_456_384,
            DABChannel::C_LD => 1_458_096,
            DABChannel::C_LE => 1_459_808,
            DABChannel::C_LF => 1_461_520,
            DABChannel::C_LG => 1_463_232,
            DABChannel::C_LH => 1_464_944,
            DABChannel::C_LI => 1_466_656,
            DABChannel::C_LJ => 1_468_368,
            DABChannel::C_LK => 1_470_080,
            DABChannel::C_LL => 1_471_792,
            DABChannel::C_LM => 1_473_504,
            DABChannel::C_LN => 1_475_216,
            DABChannel::C_LO => 1_476_928,
            DABChannel::C_LP => 1_478_640,
        }
    }

    /// Return a string representation of the DAB channel variant.
    pub fn as_str(&self) -> &'static str {
        match *self {
            DABChannel::C_5A => "5A",
            DABChannel::C_5B => "5B",
            DABChannel::C_5C => "5C",
            DABChannel::C_5D => "5D",
            DABChannel::C_6A => "6A",
            DABChannel::C_6B => "6B",
            DABChannel::C_6C => "6C",
            DABChannel::C_6D => "6D",
            DABChannel::C_7A => "7A",
            DABChannel::C_7B => "7B",
            DABChannel::C_7C => "7C",
            DABChannel::C_7D => "7D",
            DABChannel::C_8A => "8A",
            DABChannel::C_8B => "8B",
            DABChannel::C_8C => "8C",
            DABChannel::C_8D => "8D",
            DABChannel::C_9A => "9A",
            DABChannel::C_9B => "9B",
            DABChannel::C_9C => "9C",
            DABChannel::C_9D => "9D",
            DABChannel::C_10A => "10A",
            DABChannel::C_10B => "10B",
            DABChannel::C_10C => "10C",
            DABChannel::C_10D => "10D",
            DABChannel::C_11A => "11A",
            DABChannel::C_11B => "11B",
            DABChannel::C_11C => "11C",
            DABChannel::C_11D => "11D",
            DABChannel::C_12A => "12A",
            DABChannel::C_12B => "12B",
            DABChannel::C_12C => "12C",
            DABChannel::C_12D => "12D",
            DABChannel::C_13A => "13A",
            DABChannel::C_13B => "13B",
            DABChannel::C_13C => "13C",
            DABChannel::C_13D => "13D",
            DABChannel::C_13E => "13E",
            DABChannel::C_13F => "13F",

            DABChannel::C_LA => "LA",
            DABChannel::C_LB => "LB",
            DABChannel::C_LC => "LC",
            DABChannel::C_LD => "LD",
            DABChannel::C_LE => "LE",
            DABChannel::C_LF => "LF",
            DABChannel::C_LG => "LG",
            DABChannel::C_LH => "LH",
            DABChannel::C_LI => "LI",
            DABChannel::C_LJ => "LJ",
            DABChannel::C_LK => "LK",
            DABChannel::C_LL => "LL",
            DABChannel::C_LM => "LM",
            DABChannel::C_LN => "LN",
            DABChannel::C_LO => "LO",
            DABChannel::C_LP => "LP",
        }
    }
}

impl ::std::fmt::Debug for DABChannel {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

impl ::std::fmt::Display for DABChannel {
    fn fmt(&self, f: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        write!(f, "{}", self.as_str())
    }
}

#[cfg(test)]
mod tests {
    use super::DABChannel;

    #[test]
    fn can_format_dab_channels() {
        assert_eq!(format!("{}", DABChannel::C_12B), "12B");
    }

    #[test]
    fn can_debug_format_dab_channels() {
        assert_eq!(format!("{:?}", DABChannel::C_12B), "12B");
    }

}
