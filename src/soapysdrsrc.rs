//  gst-plugin-sdr — a GStreamer plugin to handle software defined radio.
//
//  Copyright © 2018, 2019  Russel Winder <russel@winder.org.uk>
//
//  gst-plugin-sdr is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  gst-plugin-sdr is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with gst-plugin-sdr.  If not, see <https://www.gnu.org/licenses/>.

use std::sync::Mutex;
use std::sync::mpsc::{Receiver, Sender, TryRecvError, channel};
use std::i32;

use byteorder::{NativeEndian, WriteBytesExt};

use glib::{glib_object_impl, glib_object_subclass};
use glib::ToValue;
use glib::object::Cast;
use glib::subclass::Property;
use glib::subclass::object::{ObjectImpl, ObjectClassSubclassExt};
use glib::subclass::simple::ClassStruct;
use glib::subclass::types::ObjectSubclass;

use gstreamer as gst;
use gst::{gst_error, gst_error_msg, gst_info, gst_log_with_level};
use gst::subclass::ElementInstanceStruct;
use gst::subclass::element::{ElementImpl, ElementClassSubclassExt};
use gstreamer_base as gst_base;
use gst_base::BaseSrc;
use gst_base::subclass::base_src::BaseSrcImpl;
use gstreamer_audio as gst_audio;

use num_complex::Complex;

use soapysdr::{Args, Device, Direction, RxStream, enumerate};

use crate::src_common::{PROPERTIES, Settings, StreamingState};

// #[derive(Debug)]
struct State {
    stream: Option<Receiver<Complex<f32>>>,
    streaming_state: StreamingState,
}

//impl State {
//}

impl Default for State {
    fn default() -> State {
        State {
            stream: None,
            streaming_state: StreamingState::Stopped,
        }
    }
}

//#[derive(Debug)]
pub struct SoapySDRSrc {
    settings: Mutex<Settings>,
    state: Mutex<State>,
    cat: gst::DebugCategory,
}

impl SoapySDRSrc {

}

impl ObjectSubclass for SoapySDRSrc {
    const NAME: &'static str = "SoapySDRSrc";
    type ParentType = BaseSrc;
    type Instance = ElementInstanceStruct<Self>;
    type Class = ClassStruct<Self>;

    glib_object_subclass!();

    fn class_init(klass: &mut ClassStruct<Self>) {
        klass.set_metadata(
            "SoapySDR Source",
            "Source/Audio",
            "Creates an IQ stream from a SoapySDR source",
            "Russel Winder <russel@winder.org.uk>",
        );

        // On the src pad, we can produce F32/F64 with any sample rate
        // and any number of channels
        let caps = gst::Caps::new_simple(
            "audio/x-raw",
            &[
                (
                    "format",
                    &gst::List::new(&[
                        &gst_audio::AUDIO_FORMAT_F32.to_string(),
                        &gst_audio::AUDIO_FORMAT_F64.to_string(),
                    ]),
                ),
                ("layout", &"interleaved"),
                ("rate", &gst::IntRange::<i32>::new(1, i32::MAX)),
                ("channels", &gst::IntRange::<i32>::new(1, i32::MAX)),
            ],
        );
        // The src pad template must be named "src" for basesrc
        // and specific a pad that is always there
        let src_pad_template = gst::PadTemplate::new(
            "src",
            gst::PadDirection::Src,
            gst::PadPresence::Always,
            &caps,
        ).expect("Could not create a new src pad.");
        klass.add_pad_template(src_pad_template);
        klass.install_properties(&PROPERTIES);
    }

    fn new() -> Self {
        Self {
            settings: Default::default(),
            state: Default::default(),
            cat: gst::DebugCategory::new("soapysdrsrc", gst::DebugColorFlags::empty(), Some("SoapySDR source")),
        }
    }
}

impl ObjectImpl for SoapySDRSrc {
    glib_object_impl!();

    fn set_property(&self, obj: &glib::Object, id: usize, value: &glib::Value) {
        let prop = &PROPERTIES[id];
        let basesrc = obj.downcast_ref::<gst_base::BaseSrc>().unwrap();
        match *prop {
            Property("frequency", ..) => {
                let mut settings = self.settings.lock().unwrap();
                let new_frequency = value.get().unwrap().unwrap();
                gst_info!(self.cat, obj: basesrc, "Changing frequency from {} to {}", settings.frequency, new_frequency);
                settings.frequency = new_frequency;
            },
            _ => unimplemented!(),
        }
    }

    fn get_property(&self, _obj: &glib::Object, id: usize) -> Result<glib::Value, ()> {
        let prop = &PROPERTIES[id];
        match *prop {
            Property("frequency", ..) => {
                let settings = self.settings.lock().unwrap();
                Ok(settings.frequency.to_value())
            },
            _ => unimplemented!(),
        }
    }
}

impl ElementImpl for SoapySDRSrc {}

impl BaseSrcImpl for SoapySDRSrc {
    fn start(&self, src: &BaseSrc) -> Result<(), gst::ErrorMessage> {
        let settings = self.settings.lock().unwrap();
        let mut state = self.state.lock().unwrap();
        if state.streaming_state == StreamingState::Started {
            unreachable!("SoapySDR source already started.");
        }
        let (sender, receiver) = channel::<Complex<f32>>();
        state.stream = Some(receiver);
        state.streaming_state = StreamingState::Started;
        std::thread::spawn({
            let s = settings.clone();
            ||{ run_device_thread(s, sender)}
        });
        Ok(())
    }

    fn stop(&self, _src: &BaseSrc) -> Result<(), gst::ErrorMessage> {
        let mut state = self.state.lock().unwrap();
        state.streaming_state = StreamingState::Stopped;
        state.stream = None;
        Ok(())
    }

    fn is_seekable(&self, _src: &BaseSrc) -> bool { false  }

    fn get_size(&self, _src: &BaseSrc) -> Option<u64> { None }

    fn fill(&self, src: &BaseSrc, _: u64, _: u32, buffer: &mut gst::BufferRef) -> Result<gst::FlowSuccess, gst::FlowError> {
        let state = self.state.lock().unwrap();
        if state.streaming_state == StreamingState::Stopped {
            gst_error!(self.cat, obj: src, "Not started yet.");
            return Err(gst::FlowError::Error);
        }
        match state.stream {
            Some(ref stream) => {
                let size = {
                    let mut map = match buffer.map_writable() {
                        Ok(map) => map,
                        Err(_) => {
                            gst_error!(self.cat, obj: src, "Failed to map buffer.");
                            return Err(gst::FlowError::Error);
                        },
                    };
                    let mut buffer = map.as_mut_slice();
                    let mut count = 0;
                    for mut i in 0..(buffer.len() - 8) { // Ensure enough space is left for adding a CF32 which is 8 bytes.
                        match stream.try_recv() {
                            Ok(value) => {
                                let mut bytes_buffer = vec![];
                                bytes_buffer.write_f32::<NativeEndian>(value.re).unwrap();
                                assert_eq!(bytes_buffer.len(), 4);
                                for j in 0..bytes_buffer.len() {
                                    buffer[i] = bytes_buffer[j];
                                    i += 1;
                                }
                                let mut bytes_buffer = vec![];
                                bytes_buffer.write_f32::<NativeEndian>(value.im).unwrap();
                                assert_eq!(bytes_buffer.len(), 4);
                                for j in 0..bytes_buffer.len() {
                                    buffer[i] = bytes_buffer[j];
                                    i += 1;
                                }
                                count = i + 1;
                            },
                            Err(error) => match error {
                                TryRecvError::Empty => break,
                                TryRecvError::Disconnected => panic!("Device/RxStream actor terminated."),
                            }
                        }
                    }
                    count
                };
                buffer.set_size(size);
                Ok(gst::FlowSuccess::Ok)
            },
            None => {
                gst_error!(self.cat, obj: src, "No stream set up as yet.");
                Err(gst::FlowError::Error)
            }
        }
    }
}

// As per ideas set out in https://github.com/kevinmehall/rust-soapysdr/issues/9
// until there are changes in Rust_SoapySDR, use a separate thread to manage the
// Device and RxStream instances so as to get the correct lifetimes and use a channel
// to send things back.

fn run_device_thread(settings: Settings, to_driver: Sender<Complex<f32>>) {
    let device_args = enumerate("").unwrap();
    let device_arg = match device_args.len() {
        0 => {
            gst_error_msg!(gst::ResourceError::Settings, ["No SoapySDR devices available."]);
            return;
        },
        1 => device_args.into_iter().next().unwrap(),
        n => {
            gst_error_msg!(gst::ResourceError::Settings, ["More than one SoapySDR devices available."]);
            return;
        },
    };
    let device = Device::new(device_arg).unwrap();
    if device.num_channels(Direction::Rx).unwrap() == 0 {
        gst_error_msg!(gst::ResourceError::Settings, ["No SoapySDR receivers available."]);
        return;
    }
    // TODO Should not assume channel 0.
    let sample_rates = device.get_sample_rate_range(Direction::Rx, 0).unwrap();
    if sample_rates.len() == 0 {
        gst_error_msg!(gst::ResourceError::Settings, ["No sample available."]);
        return;
    }
    // Set frequency before sample rate to avoid "PLL not locked!"
    device.set_frequency(Direction::Rx, 0, settings.frequency as f64, Args::new()).unwrap();
    // TODO Need to do better than just select the smallest sample rate.
    device.set_sample_rate(Direction::Rx, 0, sample_rates[0].minimum).unwrap();
    // TODO must check that CF32 is a valid stream format.
    let mut stream = device.rx_stream::<Complex<f32>>(&[0]).unwrap();
    stream.activate(None).unwrap();
    let mtu = stream.mtu().unwrap();
    loop {
        let mut buffer = vec!(Complex::new(0.0, 0.0); mtu);
        match stream.read(&[&mut buffer], 0) {
            Ok(count) => {
                for i in 0..count {
                    to_driver.send(buffer.get(i).unwrap().clone()).unwrap();
                }
            },
            Err(err) => panic!("Failed to read: {:?}", err),
        }
    }
    stream.deactivate(None).unwrap();
}

pub fn register(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    gst::Element::register(Some(plugin), "soapysdrsrc", gst::Rank::None, SoapySDRSrc::get_type())
}

#[cfg(test)]
mod tests {

    use super::{State, StreamingState};

    #[test]
    fn default_state_sets_expected_values() {
        let state: State = Default::default();
        assert!(state.stream.is_none());
        assert_eq!(state.streaming_state, StreamingState::Stopped);
    }

}
