//  gst-plugin-sdr — a GStreamer plugin to handle software defined radio.
//
//  Copyright © 2018, 2019  Russel Winder <russel@winder.org.uk>
//
//  gst-plugin-sdr is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  gst-plugin-sdr is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with gst-plugin-sdr.  If not, see <https://www.gnu.org/licenses/>.

use std::i32;

use glib::{glib_object_impl, glib_object_subclass};
use glib::subclass::Property;
use glib::subclass::object::{ObjectImpl, ObjectClassSubclassExt};
use glib::subclass::simple::ClassStruct;
use glib::subclass::types::ObjectSubclass;

use gstreamer as gst;
use gst::{gst_debug, gst_info, gst_log_with_level};
use gst::subclass::element::{ElementImpl, ElementClassSubclassExt};
use gstreamer_base as gst_base;
use gst_base::BaseTransform;
use gst_base::subclass::prelude::*;
use gstreamer_audio as gst_audio;

pub static PROPERTIES: [Property; 1] = [
    Property("frequency", |x| {
        glib::ParamSpec::uint(
            "frequency",
            "Frequency",
            "Frequency",
            1_000,
            1_000_000,
            105_400,
            glib::ParamFlags::READWRITE,
        )
    }),
];

struct IqDemodulator {
    cat: gst::DebugCategory,
}

impl IqDemodulator {

}

impl ObjectSubclass for IqDemodulator {
    const NAME: &'static str = "IQ_Demodulator";
    type ParentType = gst_base::BaseTransform;
    type Instance = gst::subclass::ElementInstanceStruct<Self>;
    type Class = ClassStruct<Self>;

    glib_object_subclass!();

    fn new() -> Self {
        Self {
            cat: gst::DebugCategory::new(
                "iq_demodulator",
                gst::DebugColorFlags::empty(),
                Some("IQ Demodulator"),
            ),
        }
    }

    fn class_init(klass: &mut ClassStruct<Self>) {
        klass.set_metadata(
            "IQ Demodulator",
            "Demodulator",
            "Converts the input sequence of complex numbers into amplitude and phase data.",
            "Russel Winder <russel@winder.org.uk>",
        );

        let caps = gst::Caps::new_simple(
            "video/x-raw",
            &[
                (
                    "format",
                    &gst::List::new(&[
                        &gst_audio::AUDIO_FORMAT_F32.to_string(),
                    ]),
                ),
                ("width", &gst::IntRange::<i32>::new(0, i32::MAX)),
                ("height", &gst::IntRange::<i32>::new(0, i32::MAX)),
                (
                    "framerate",
                    &gst::FractionRange::new(
                        gst::Fraction::new(0, 1),
                        gst::Fraction::new(i32::MAX, 1),
                    ),
                ),
            ],
        );
        let src_pad_template = gst::PadTemplate::new(
            "src",
            gst::PadDirection::Src,
            gst::PadPresence::Always,
            &caps,
        ).unwrap();
        klass.add_pad_template(src_pad_template);
        let caps = gst::Caps::new_simple(
            "video/x-raw",
            &[
                ("format", &gst_audio::AUDIO_FORMAT_F32.to_string()),
                ("width", &gst::IntRange::<i32>::new(0, i32::MAX)),
                ("height", &gst::IntRange::<i32>::new(0, i32::MAX)),
                (
                    "framerate",
                    &gst::FractionRange::new(
                        gst::Fraction::new(0, 1),
                        gst::Fraction::new(i32::MAX, 1),
                    ),
                ),
            ],
        );
        let sink_pad_template = gst::PadTemplate::new(
            "sink",
            gst::PadDirection::Sink,
            gst::PadPresence::Always,
            &caps,
        ).unwrap();
        klass.add_pad_template(sink_pad_template);
        klass.install_properties(&PROPERTIES);
        klass.configure(
            gst_base::subclass::BaseTransformMode::NeverInPlace,
            false,
            false,
        );
    }
}

impl ObjectImpl for IqDemodulator {
    glib_object_impl!();

    fn set_property(&self, obj: &glib::Object, id: usize, value: &glib::Value) {
        let prop = &PROPERTIES[id];
        //let element = obj.downcast_ref::<gst_base::BaseTransform>().unwrap();
        match *prop {
            _ => unimplemented!(),
        }
    }

    fn get_property(&self, _obj: &glib::Object, id: usize) -> Result<glib::Value, ()> {
        let prop = &PROPERTIES[id];
        match *prop {
            _ => unimplemented!(),
        }
    }
}

impl ElementImpl for IqDemodulator {}

impl BaseTransformImpl for IqDemodulator {

    fn transform_caps(
        &self,
        element: &gst_base::BaseTransform,
        direction: gst::PadDirection,
        caps: &gst::Caps,
        filter: Option<&gst::Caps>,
    ) -> Option<gst::Caps> {
        let other_caps = if direction == gst::PadDirection::Src {
            let mut caps = caps.clone();
            for s in caps.make_mut().iter_mut() {
                //s.set("format", &gst_video::VideoFormat::Bgrx.to_string());
            }
            caps
        } else {
            let mut gray_caps = gst::Caps::new_empty();
            {
                // TODO make stuff gray.
                //let gray_caps = gray_caps.get_mut().unwrap();
                //for s in caps.iter() {
                //    let mut s_gray = s.to_owned();
                //    s_gray.set("format", &gst_video::VideoFormat::Gray8.to_string());
                //    gray_caps.append_structure(s_gray);
                //}
                //gray_caps.append(caps.clone());
            }
            gray_caps
        };
        gst_debug!(
            self.cat,
            obj: element,
            "Transformed caps from {} to {} in direction {:?}",
            caps,
            other_caps,
            direction
        );
        if let Some(filter) = filter {
            Some(filter.intersect_with_mode(&other_caps, gst::CapsIntersectMode::First))
        } else {
            Some(other_caps)
        }
    }

    fn get_unit_size(&self, _element: &gst_base::BaseTransform, caps: &gst::Caps) -> Option<usize> {
        None
    }

    fn set_caps(
        &self,
        element: &gst_base::BaseTransform,
        incaps: &gst::Caps,
        outcaps: &gst::Caps,
    ) -> Result<(), gst::LoggableError> {
        gst_debug!(
            self.cat,
            obj: element,
            "Configured for caps {} to {}",
            incaps,
            outcaps
        );
        Ok(())
    }

    fn stop(&self, element: &gst_base::BaseTransform) -> Result<(), gst::ErrorMessage> {
        //let _ = self.state.lock().unwrap().take();
        gst_info!(self.cat, obj: element, "Stopped");
        Ok(())
    }

    fn transform(
        &self,
        element: &gst_base::BaseTransform,
        inbuf: &gst::Buffer,
        outbuf: &mut gst::BufferRef,
    ) -> Result<gst::FlowSuccess, gst::FlowError> {
        //let settings = *self.settings.lock().unwrap();
        //let mut state_guard = self.state.lock().unwrap();
        //let state = state_guard.as_mut().ok_or_else(|| {
        //    gst_element_error!(element, gst::CoreError::Negotiation, ["Have no state yet"]);
        //    gst::FlowError::NotNegotiated
        //})?;
        Ok(gst::FlowSuccess::Ok)
    }
}

pub fn register(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    gst::Element::register(Some(plugin), "iq_demodulator", gst::Rank::None, IqDemodulator::get_type())
}

