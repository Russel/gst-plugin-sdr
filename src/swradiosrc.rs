//  gst-plugin-sdr — a GStreamer plugin to handle software defined radio.
//
//  Copyright © 2018, 2019  Russel Winder <russel@winder.org.uk>
//
//  gst-plugin-sdr is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  gst-plugin-sdr is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with gst-plugin-sdr.  If not, see <https://www.gnu.org/licenses/>.

use std::fs::{File, OpenOptions};
use std::i32;
use std::os::unix::io::AsRawFd;
use std::sync::Mutex;

use glib::{glib_object_impl, glib_object_subclass};
use glib::ToValue;
use glib::object::Cast;
use glib::subclass::Property;
use glib::subclass::object::{ObjectImpl, ObjectClassSubclassExt};
use glib::subclass::simple::ClassStruct;
use glib::subclass::types::ObjectSubclass;

use gstreamer as gst;
use gst::{gst_debug, gst_error, gst_error_msg, gst_info, gst_log_with_level};
use gst::subclass::ElementInstanceStruct;
use gst::subclass::element::{ElementImpl, ElementClassSubclassExt};
use gstreamer_base as gst_base;
use gst_base::BaseSrc;
use gst_base::subclass::base_src::BaseSrcImpl;
use gstreamer_audio as gst_audio;

use glob::glob;
use nix::{ioc, ioctl_read, ioctl_readwrite, ioctl_readwrite_buf, ioctl_write_buf};
use num_complex::Complex;
use v4l2_sys;

use crate::src_common::{PROPERTIES, Settings, StreamingState};

const V4L2_IOC_MAGIC: u8 = b'V';
ioctl_read!(vidioc_querycap, V4L2_IOC_MAGIC, 0, v4l2_sys::v4l2_capability);
ioctl_read!(vidioc_g_fmt, V4L2_IOC_MAGIC, 4, v4l2_sys::v4l2_format);
ioctl_readwrite_buf!(vidioc_s_fmt, V4L2_IOC_MAGIC, 5, v4l2_sys::v4l2_format);
ioctl_readwrite!(vidioc_g_tuner, V4L2_IOC_MAGIC, 29, v4l2_sys::v4l2_tuner);
ioctl_write_buf!(vidioc_s_tuner, V4L2_IOC_MAGIC, 30, v4l2_sys::v4l2_tuner);
ioctl_readwrite!(vidioc_g_frequency, V4L2_IOC_MAGIC, 56, v4l2_sys::v4l2_frequency);
ioctl_write_buf!(vidioc_s_frequency, V4L2_IOC_MAGIC, 57, v4l2_sys::v4l2_frequency);

#[derive(Debug)]
struct State {
    device: Option<File>,
    device_number: u8,
    streaming_state: StreamingState,
}

//impl State {
//}

impl Default for State {
    fn default() -> State {
        State {
            device: None,
            device_number: 0,
            streaming_state: StreamingState::Stopped,
        }
    }
}

#[derive(Debug)]
pub struct SWRadioSrc {
    settings: Mutex<Settings>,
    state: Mutex<State>,
    cat: gst::DebugCategory,
}

impl SWRadioSrc {

}

impl ObjectSubclass for SWRadioSrc {
    const NAME: &'static str = "SWRadioSrc";
    type ParentType = BaseSrc;
    type Instance = ElementInstanceStruct<Self>;
    type Class = ClassStruct<Self>;

    glib_object_subclass!();

    fn class_init(klass: &mut ClassStruct<Self>) {
        klass.set_metadata(
            "SWRadio Source",
            "Source/Audio",
            "Creates an IQ stream from a SWRadio source",
            "Russel Winder <russel@winder.org.uk>",
        );

        // On the src pad, we can produce F32/F64 with any sample rate
        // and any number of channels
        let caps = gst::Caps::new_simple(
            "audio/x-raw",
            &[
                (
                    "format",
                    &gst::List::new(&[
                        &gst_audio::AUDIO_FORMAT_F32.to_string(),
                        &gst_audio::AUDIO_FORMAT_F64.to_string(),
                    ]),
                ),
                ("layout", &"interleaved"),
                ("rate", &gst::IntRange::<i32>::new(1, i32::MAX)),
                ("channels", &gst::IntRange::<i32>::new(1, i32::MAX)),
            ],
        );
        // The src pad template must be named "src" for basesrc
        // and specific a pad that is always there
        let src_pad_template = gst::PadTemplate::new(
            "src",
            gst::PadDirection::Src,
            gst::PadPresence::Always,
            &caps,
        ).expect("Could not create src pad.");
        klass.add_pad_template(src_pad_template);
        klass.install_properties(&PROPERTIES);
    }

    fn new() -> Self {
        Self {
            settings: Mutex::new(Default::default()),
            state: Mutex::new(Default::default()),
            cat: gst::DebugCategory::new("swradiosrc", gst::DebugColorFlags::empty(), Some("SWRadio source")),
        }
    }
}

impl ObjectImpl for SWRadioSrc {
    glib_object_impl!();

    fn set_property(&self, obj: &glib::Object, id: usize, value: &glib::Value) {
        let prop = &PROPERTIES[id];
        let basesrc = obj.downcast_ref::<gst_base::BaseSrc>().unwrap();
        match *prop {
            Property("frequency", ..) => {
                let mut settings = self.settings.lock().unwrap();
                let new_frequency = value.get().unwrap().unwrap();
                gst_info!(self.cat, obj: basesrc, "Changing frequency from {} to {}", settings.frequency, new_frequency);
                settings.frequency = new_frequency;
            },
            _ => unimplemented!(),
        }
    }

    fn get_property(&self, _obj: &glib::Object, id: usize) -> Result<glib::Value, ()> {
        let prop = &PROPERTIES[id as usize];
        match *prop {
            Property("frequency", ..) => {
                let settings = self.settings.lock().unwrap();
                Ok(settings.frequency.to_value())
            },
            _ => unimplemented!(),
        }
    }
}

impl ElementImpl for SWRadioSrc { }

impl BaseSrcImpl for SWRadioSrc {
    fn start(&self, src: &BaseSrc) -> Result<(), gst::ErrorMessage> {
        let mut state = self.state.lock().unwrap();
        if state.streaming_state == StreamingState::Started {
            unreachable!("SWRadio source already started.");
        }
        let paths = match glob("/dev/swradio*") {
            Ok(paths) => paths,
            Err(e) => { return Err(gst_error_msg!(gst::ResourceError::Settings, ["No SWRadio devices available."])); },
        };
        // TODO remove the assumption of only one radio device.
        let device = OpenOptions::new()
            .write(true)
            .read(true)
            .open(&format!("/dev/swradio{}", state.device_number)).expect("Cannot open the special device.");
        let mut capabilities = v4l2_sys::v4l2_capability{
            driver: [0u8; 16],
            card: [0u8; 32],
            bus_info: [0u8; 32],
            version: 0,
            capabilities: 0,
            device_caps: 0,
            reserved: [0u32; 3],
        };
        unsafe {
            vidioc_querycap(device.as_raw_fd(), &mut capabilities).expect("vidioc_querycap failed.");
        }
        if capabilities.device_caps & v4l2_sys::V4L2_CAP_TUNER == 0
            || capabilities.device_caps & v4l2_sys::V4L2_CAP_SDR_CAPTURE == 0
            || capabilities.device_caps & v4l2_sys::V4L2_CAP_STREAMING == 0 {
            return Err(gst_error_msg!(gst::ResourceError::Settings, ["Device seems to have the wrong capabilities."]));
        }
        let mut tuner = v4l2_sys::v4l2_tuner {
            index: 0,
            name: [0; 32],
            type_: 0,
            capability: 0,
            rangelow: 0,
            rangehigh: 0,
            rxsubchans:0,
            audmode: 0,
            signal: 0,
            afc: 0,
            reserved: [0; 4],
        };
        unsafe {
            vidioc_g_tuner(device.as_raw_fd(), &mut tuner).expect("vidioc_g_tuner failed.");
        }
        let mut freq_buffer = v4l2_sys::v4l2_frequency{
            tuner: 0,
            frequency: 0,
            type_: v4l2_sys::v4l2_tuner_type_V4L2_TUNER_RADIO,
            reserved: [0; 8],
        };
        unsafe {
            vidioc_g_frequency(device.as_raw_fd(), &mut freq_buffer).expect("vidioc_g_frequency failed.");
        }
        gst_debug!(self.cat, obj: src, "Opened SWRadio stream");
        state.device = Some(device);
        state.streaming_state = StreamingState::Started;
        Ok(())
    }

    fn stop(&self, _src: &BaseSrc) -> Result<(), gst::ErrorMessage> {
        let mut state = self.state.lock().unwrap();
        assert_eq!(state.streaming_state, StreamingState::Started);
        state.streaming_state = StreamingState::Stopped;
        assert!(state.device.is_some());
        // TODO what happens to the device object at this point?
        state.device = None;
        Ok(())
    }

    fn fill(&self, element: &BaseSrc, _offset: u64, _length: u32, buffer: &mut gst::BufferRef) -> Result<gst::FlowSuccess, gst::FlowError> {
        let state = self.state.lock().unwrap();
        if state.streaming_state == StreamingState::Stopped {
            gst_error!(self.cat, obj: element, "Not started yet");
            return Err(gst::FlowError::Error);
        }
        let size = {
            let mut map = match buffer.map_writable() {
                Ok(map) => map,
                Err(_) => {
                    gst_error!(self.cat, obj: element, "Failed to map buffer");
                    return Err(gst::FlowError::Error);
                },
            };
            let mut buffer = map.as_mut_slice();
            // TODO Read something in!
            //state.stream.read(&[&mut buffer], self.stream.mtu.unwrap()).or_else(|err| {
            //    gst_error!(self.cat, obj: element, "Failed to read: {:?}", err);
            //    return gst::FlowReturn::Error;
            //})?
            0
        };
        buffer.set_size(size);
        Ok(gst::FlowSuccess::Ok)
    }
}

pub fn register(plugin: &gst::Plugin) -> Result<(), glib::BoolError> {
    gst::Element::register(Some(plugin), "swradiosrc", gst::Rank::None, SWRadioSrc::get_type())
}

#[cfg(test)]
mod tests {

    use super::{State, StreamingState};

    #[test]
    fn default_state_sets_expected_values() {
        let state: State = Default::default();
        assert!(state.device.is_none());
        assert_eq!(state.device_number, 0);
        assert_eq!(state.streaming_state, StreamingState::Stopped);
    }

}
