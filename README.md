# gst-plugin-sdr

[![Build Status](https://travis-ci.org/russel/gst-plugin-sdr.svg?branch=master)](https://travis-ci.org/russel/gst-plugin-sdr)
[![Licence](https://img.shields.io/badge/license-LGPL_3-green.svg)](https://www.gnu.org/licenses/lgpl-3.0.txt)

This is a [GStreamer](https://gstreamer.freedesktop.org/) plugin with elements for processing FM or DAB/DAB+
radio streams.

_*This is currently very much an early stage experiment.*_

There are two sources here, one working with the Linux kernel RTLSDR driver via /dev/swradioX, and one that
relies on SoapySDR to connect to a device. Both deliver a stream of complex numbers that is fed into common
decoding software. The decoding software delivers an AAC stream for DAB+ stations and MPEG-1 level 2 for DAB
stations. It also delivers ETI packets on another pad.

## Other work

There are a number of other projects on related activity.

For full players, that include decoding, there is:

* WelleIO
* Qt_DAB
* DABlin

For software libraries there are:

* DABtools (now unmaintained)
* sdrdab
* eti-tools

All of these are using C and/or C++ for implementation.

## Licence

This code is licenced under LGPLv3.
[![Licence](https://www.gnu.org/graphics/lgplv3-88x31.png)](https://www.gnu.org/licenses/lgpl-3.0.txt)
