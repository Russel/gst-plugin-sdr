//  gst-plugin-sdr — a GStreamer plugin to handle software defined radio.
//
//  Copyright © 2018, 2019  Russel Winder <russel@winder.org.uk>
//
//  gst-plugin-sdr is free software: you can redistribute it and/or modify
//  it under the terms of the GNU Lesser General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  gst-plugin-sdr is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public License
//  along with gst-plugin-sdr.  If not, see <https://www.gnu.org/licenses/>.

use gstreamer as gst;
use gstreamer_check as gst_check;

use gstsdr;

// Taken from GST_Plugin_Rs/src/gst-plugin-threadshare/tests/appsrc.rs
fn init() {
    use std::sync::{Once, ONCE_INIT};
    static INIT: Once = ONCE_INIT;

    INIT.call_once(|| {
        gst::init().unwrap();
        gstsdr::plugin_register_static().unwrap();
    });
}

#[test]
fn can_get_swradiosrc_element() {
    init();
    let mut harness = gst_check::Harness::new("swradiosrc");
    let element = harness.get_element().unwrap();
}

#[test]
fn can_get_soapysdrsrc_element() {
    init();
    let mut harness = gst_check::Harness::new("soapysdrsrc");
    let element = harness.get_element().unwrap();
}
